import Vue from 'vue';

export const loadData = ({commit}) => {
  Vue.http.get('data-stock-trader.json')
  .then(response => response.json())
  .then(data =>{
    const stocks = data.stocks;
    const funds = data.funds;
    const stockPortfolio = data.stockPortfolio;

    const portFolio = {
      stockPortfolio,
      funds
    };
    commit('SET_STOCKS', stocks);
    commit('SET_PORTFOLIO', portFolio);
  })
}